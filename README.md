# Mediacentre

A simple mediacentre running in a webapp that organises and displays movies and shows.

## How ~~does~~ will it work for the user

`mediacentre` will look into a local folder and try to find movies and tv shows. Based on the
configuration of the app, it may ignore videos that are too short, or completely discard some
file extension.

The webapp will try to recognise what the video is, by parsing the filename and the metadata.
Based on these information, it may gather mutliple videos together (belonging to the same TV
show for instance), look for film poster, search for synopsys on Wikipedia, etc.

The user will see all the videos and will be able to explore the content of the folder using
the arrows of the keyboard (or the arrows on the remote controller). By pressing the "Play" button,
the video will immediatly start. If it's a TV-show, it will play the next episode in the waiting
list. Otherwise, the user can press "Enter" to display more information, choose the episode to
watch, etc.

Depending on the configuration, the video may play directly into the webapp, or be played with
the default application on the machine (e.g., VLC). Because the source folder may be SSH mounted
file system, it is also possible to configure the application to copy the file somewhere else on
the machine locally, and to keep the files locally for a given period of time since last time it
was played. If you have kids, you'll know that the same video can be played literally 100 times
and it would be a waste of data transfer to stream the file every time.

## Interface (prospective)

### The homepage
![Interface of the homepage as I would like to do. There is a bunch of movie posters and tv show poster.
The interface is very simple, with a title, a parental lock on the upper right corner and all the
poster sorted by alphabetical order; a blue mark indicates that a video has not been watched yet. Watched
videos are put at the end of the list, in alphabetical order.](https://gitlab.com/ohne_sonne/mediacenter/-/raw/6854d9c3fa44176b11662eb0c42350c66ef21d1c/resources/readme_homepage.png)

## What else ~~can~~ will it do?

Here are some features I would like to implement:
- Play a *summary* of the video (10 segments of 3 seconds each) to quickly remind you what happened
  in the last episode,
- Use `scp` to copy the file locally rather than streaming the files; resume copy if interrupted
  without perturbing the viewing,
- Automatically download subtitles (and automatically align the subtitles, that's another project),
- Automatically find standardised metadata about the video based on filename, metadata and hash,
- Allow user to fill in metadata to help/correct the application,
- Password protect videos (black/white list, so kids don't watch the wrong movie by mistake).


## Can I help?

YES! This is not an easy task, so I would love to get some help :)

Please do not hesitate to contact me if you are interested in helping.

## Why Svelte?

Ok, I must admit, I really want to learn Svelte and modern way of building a web application
and website. 10 years ago, I was developing website for a stupid company, everything was done
by hand: html, javascript, css, etc. Before the social networks, Javascript was disabled BY DEFAULT
on many computers, and the web was pretty much statics. The venue of javascript and standardisation
of the CSS was coming very very slowly. I spent a fair amount of months (18 to be precise) developing
website in full Flash. That was a ridiculous time.

Things are so much better now. And I realise one thing recently: I don't know how to make a website
anymore. Sure, I can open Vim and type HTML5 out of my head, do everything in pure JS and use tricky
hack to display things correctly for all browser, but don't ask me to make things compatible for
every devices, touch screen, mouse, arial, and so many others things that were completely ignored
or inexistant back then. A website on one page? Naaah, that's a nightmare to write by hand. Of course
you need something to do it for you.

Anyway, I need a mediacenter, and I want to stay up-to-date when it comes to webdev. So here I am,
learning Svelte so my kid can enjoy the shows I have ~~downloaded~~ purchased legally. I also noted
that React, Vue, etc... are pain in the ass. Already quite old, they are big machinery soon to be
obsolete. Svelte seems much more promising, and people are reasonably happy when they talk about it.
(talk about React to a React developper, and you will see that React is not a good option).
